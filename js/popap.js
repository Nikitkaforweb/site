$(document).ready(function() {

	$('#security__button').click(function() {
		$('#popap, #overlay').fadeIn(200);
	});

	$('#close_popap').click(function() {
		$('#popap, #overlay').fadeOut(200);
	});

	$('#overlay').click(function() {
		$('#popap, #overlay').fadeOut(200);
	});
});