jQuery(document).ready(function ($) {
    setInterval(function () {
        switchNext();
    }, 5000);
 
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
             $('#slider ul li:last-child').prependTo('#slider ul');

    function switchPrev() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 700, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function switchNext() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 700, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.btn_prev').click(function () {
        switchPrev();
    });

    $('a.btn_next').click(function () {
        switchNext();
    });
});