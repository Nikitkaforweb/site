<?php
	require_once('db.php');
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Пан Пес</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/clogin.css">

	<!-- ШРИФТЫ -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ruslan+Display&amp;subset=cyrillic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Underdog&amp;subset=cyrillic" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
	<!-- СКРИПТЫ -->
	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script> 
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/popap.js"></script>
			
</head>
<body>



	<?php
		require "php/popap.php";
	?>

	
	
	<!-- HEADER -->

	<header class="header">
		<div class="header__content">


		
			<div class="security">

				<div class="security">
					<?php
					if(isset($_SESSION["id"])) {

					?>
					<span style="color: white;"> <?php echo $_SESSION["login"]; ?> </span>
					<br>
					<a style="color: white;" href="php/logout.php" font-color="white">Выйти</a>
					<?php } else { ?>
					<button id="security__button"></button>
					<?php } ?>
					</div>
								
				</div>	


			<div class="header__logo">
				<a href="#navigation"></a>
				<p class="header__text">Интернет зоомагазин</p>
				<h1 class="header__text--h">„ПАН ПЕС“</h1>
			</div>
		</div>
	</header>

	<!-- END OF HEADER -->

	<!-- MENU -->
		<nav id="navigation">
					<ul class="menu">
						<li class="menu__list"><a class="menu__item" href="index.php"><p>ГЛАВНАЯ</p></a></li>
						<li class="menu__list"><a class="menu__item" href="html/news.php"><p>НОВОСТИ</p></a></li>
						<li class="menu__list"><a class="menu__item" href="html/goods.php"><p>ТОВАРЫ</p></a>
							<ul class="submenu">
								<li><a href="html/goods.php"><p>ТОВАРЫ</p></a></li>
								<li><a href="#"><p>ОПЛАТА</p></a></li>
							</ul>
						</li>
						<li class="menu__list"><a class="menu__item" href="#"><p>МОЙ ПРОФИЛЬ</p></a></li>
						<li class="menu__list"><a class="menu__item" href="html/contacts.php"><p>КОНТАКТЫ</p></a></li>
						<li class="menu__list"><a class="menu__item" href="html/registration.php"><p>РЕГИСТРАЦИЯ</p></a></li>
					</ul>

					<img class="ldog-img" src="img/ldog.jpg">
					<img class="rdog-img" src="img/rdog.jpg">
		</nav>
	<!-- END OF MENU -->

			<div class="main">
				<div class="sidebar">
					<ul class="sidebar__menu">
						<li class="sidebar__menu--list"><a class="sidebar__menu--item" href="html/goods.php #dog"><p>товары для собак</p></a></li>
						<li class="sidebar__menu--list"><a class="sidebar__menu--item" href="html/goods.php #cat"><p>товары для кошек</p></a></li>
						<li class="sidebar__menu--list"><a class="sidebar__menu--item" href="html/goods.php #griz"><p>товары для грызунов</p></a></li>
		
					</ul>
		

					<?php

					require "php/canvas.php";

					?>
			

				</div>


				<div class="video">
					<div class="text_for_video">
						<p>Video</p>	
					</div>

					<iframe width="560" height="315" src="https://www.youtube.com/embed/I-R2ebnXVog" frameborder="0" allowfullscreen class="video__position"></iframe>
				</div>


				<aside class="reklama">
					<div class="reklama__vulkan">
						<button class="vulkan__button" onClick="window.open('http://natribukvy.ru/', ' _parent','height=550, width=800');">
							
						</button>
						<a class="vulkan__text" href="http://natribukvy.ru/">
							Вулкан ставки
						</a>
					</div>

					<div class="reklama__joycasino">
						<button class="joycasino__button" onClick="window.open('http://заебал.рф/', ' _parent','height=550, width=800');">
							
						</button>
						<a class="joycasino__text" href="http://заебал.рф/">
							Ставки на спорт
						</a>
					</div>
				</aside>
			</div>


		<?php

		require "php/slider.php";

		?>



			<div class="sponsors">
			<hr>
			<img src="img/fotLogos.png" alt="спонсоры" class="sponsors__img">
			
			</div>

			<footer class="footer">
				<div class="footer__content">
						<img  class="footer__logo" src="img/flogo.png" alt="click me">
						<p class="footer__fraza">
							Домашние животные украшают нашу жизнь, а в трудную минуту стол.
						</p>
						<p class="footer__phone">
					 		Контактный телефон: (066)327-68-83
						</p>
						
				</div>
			</footer>
	<div id="overlay"></div>
</body>
</html>

