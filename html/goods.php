<?php
  require "../db.php";

?>
<!DOCTYPE html>
<html>
<head>
  <title>Товары</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/goods.css">
  <link rel="stylesheet" type="text/css" href="../css/clogin.css">

  <!-- ШРИФТЫ -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ruslan+Display&amp;subset=cyrillic" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Underdog&amp;subset=cyrillic" rel="stylesheet"> 
  <link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
  <!-- СКРИПТЫ -->
  <script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script> 
  <script type="text/javascript" src="../js/popap.js"></script>

</head>
<body>



<!-- popap -->

<!-- <?php
	// require("../php/popap.php")
?> -->
	
<!-- end popap -->


  <!--HEADER-->
<?php
	require("../php/header.php")
?>

  <!--END OF HEADER-->

  <!--MENU-->

<?php
	require("../php/menu.php");
?>

  <!-- END OF MENU -->


<div class="forgoods">
	<div class="goods__for__dogs" id="dog">
		<fieldset class="for__dogs">
			<legend align="center">For dogs</legend>
<a href="">
			<div class="for_first">
				<div class="name__good">
					<p>ROYAL CANIN</p>
				</div>
				<div class="id__good">
					<p>id: 000-001</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good1_dog.png">
				</div>
				<div class="text__good">
					<p>Сухой корм супер-премиум класса с лососем и деликатным соусом для взрослых собак всех пород, привередливых в еде.</p>
				</div>
			</div>
</a>
<a href="">
			<div class="for_second">
				<div class="name__good">
					<p>Pedigree</p>
				</div>
				<div class="id__good">
					<p>id: 000-002</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good2_dog.png">
				</div>
				<div class="text__good">
					<p>Корм для взрослых активных собак на основе мяса курицы и риса.</p>
				</div>
			</div>
</a>
<a href="">
			<div class="for_third">
				<div class="name__good">
					<p>Eukanuba</p>
				</div>
				<div class="id__good">
					<p>id: 000-003</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good3_dog.png">
				</div>
				<div class="text__good">
					<p>Полнорационный сухой корм с ягненком и рисом для взрослых собак.</p>
				</div>
			</div>
</a>
		</fieldset>
	</div>




	<div class="goods__for__cats" id="cat">
		<fieldset class="for__cats">
			<legend align="center">For cats</legend>
			<a href="">
			<div class="for_first">
				<div class="name__good">
					<p>Josera Cat Culinesse</p>
				</div>
				<div class="id__good">
					<p>id: 001-001</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good1_cat.jpg">
				</div>
				<div class="text__good">
					<p>Josera Culinesse - корм супер-премиум класса, специально разработан для взрослых кошек.</p>
				</div>
			</div>
</a>
<a href="">
			<div class="for_second">
				<div class="name__good">
					<p>Optimeal Cat Adult Chicken</p>
				</div>
				<div class="id__good">
					<p>id: 001-002</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good2_cat.jpg">
				</div>
				<div class="text__good">
					<p>Optimeal Cat Adult Chicken - полноценное питание на основе мяса курицы для взрослых кошек. </p>
				</div>
			</div>
</a>
<a href="">
			<div class="for_third">
				<div class="name__good">
					<p>Optimeal Kittens</p>
				</div>
				<div class="id__good">
					<p>id: 001-003</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good3_cat.jpg">
				</div>
				<div class="text__good">
					<p>Optimeal Kittens - полнорационная формула на основе куриного мяса для котят до 12-и месяцев. </p>
				</div>
			</div>
</a>
		</fieldset>
	</div>	




	<div class="goods__for__grizyni" id="griz">
		<fieldset class="for__grizyni">
			<legend align="center">For grizyni</legend>
			<a href="">
			<div class="for_first">
				<div class="name__good">
					<p>Vitakraft Pellets для шиншилл</p>
				</div>
				<div class="id__good">
					<p>id: 002-001</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good1_griz.png">
				</div>
				<div class="text__good">
					<p>Корм для шиншилл высокого качества, содержащий много клетчатки, витамины и минералы, обогащен витаминами и ферментами солода.</p>
				</div>
			</div>
</a>
<a href="">
			<div class="for_second">
				<div class="name__good">
					<p>Versele-Laga Nature Cuni </p>
				</div>
				<div class="id__good">
					<p>id: 002-002</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good2_griz.jpg">
				</div>
				<div class="text__good">
					<p>Корм суперпремиум класса из натуральных продуктов, тщательно отобранных и обогащенных люцерной для декоративных кроликов.</p>
				</div>
			</div>
</a>
<a href="">
			<div class="for_third">
				<div class="name__good">
					<p>Vitakraft Vita Special All Ages</p>
				</div>
				<div class="id__good">
					<p>id: 002-003</p>
				</div>
				<div class="img__good">
					<img src="../img/goods/good3_griz.jpg">
				</div>
				<div class="text__good">
					<p>Полнорационный корм для поддержания здоровья пищеварительной системы шиншилл в любом возрасте.</p>
				</div>
			</div>
</a>
		</fieldset>
	</div>	
</div>

 <!-- FOOTER -->
  	<footer class="footer">
  		<div class="footer__content">
  			<p class="footer__fraza">			 
				Какие же эти животные милые и приятные на вкус!!!
			</p>

			<?php
			require("../php/footer.php")
			?>