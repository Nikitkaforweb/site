<?php
  require "../db.php";

?>


<!DOCTYPE html>
<html>
<head>
  <title>Регистрация</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../css/registration.css">
  <link rel="stylesheet" type="text/css" href="../css/clogin.css">

  <!-- ШРИФТЫ -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ruslan+Display&amp;subset=cyrillic" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Underdog&amp;subset=cyrillic" rel="stylesheet"> 
  <link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
  <!-- СКРИПТЫ -->
  <script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script> 
  <script type="text/javascript" src="../js/popap.js"></script>
  <script type="text/javascript" src="../js/file_loader.js"></script>

</head>
<body>


<!-- popap -->

<!-- <?php
  // require("../php/popap.php");
?> -->
  
<!-- end popap -->


  <!--HEADER-->
<?php
  require("../php/header.php");
?>

  <!--END OF HEADER-->

  <!--MENU-->

<?php
  require("../php/menu.php");
?>

  <!-- END OF MENU -->



  <fieldset class="registration">
  	<legend align="center">Регистрация</legend>


    <?php
      require ("../php/registration.php");
    ?>


    <form action="" method="POST">

    	<div class="registration__left">
    		<p class="registration__text">Аватарка:</p>
    		<div class="registration__left--img">
    			<img src="../img/loginpeople.png" id="imgpreview" width="250px" height="300px">
    		</div>
    		<div class="registration__left--photogruz">
    			<input type="file" name="photo" accept="image/*" id="image_loader" onchange="imagepreview(this);">
    			 <input type="submit" value="Отправить"></p>
    		</div>
    	</div>

    	<div class="registration__right">


    		<p class="registration__titlefilds">Введите логин:</p>
    		<input type="text" name="login" class="registration__input" value="<?php echo @$data['login'] ; ?>">


    		<p class="registration__titlefilds">Введите имя:</p>
    		<input type="text" name="name" class="registration__input" value="<?php echo @$data['name'] ; ?>">


    		<p class="registration__titlefilds">Введите mail:</p>
    		<input type="mail" name="mail" class="registration__input" value="<?php echo @$data['mail'] ; ?>">


    		<p class="registration__titlefilds">Повторите mail:</p>
    		<input type="mail" name="mail_2" class="registration__input" value="<?php echo @$data['mail_2'] ; ?>">


    		<p class="registration__titlefilds">Введите пароль:</p>
    		<input type="password" name="password" class="registration__input">


    		<p class="registration__titlefilds">Повторите пароль:</p>
    		<input type="password" name="password_2" class="registration__input">


    		<p class="registration__titlefilds">Выберите пол:</p>
    		<input type="radio" name="sex" id="man" class="sex">
    		<label for="man" class="sex_man" va>Mужчина</label>
    		<input type="radio" name="sex" id="woman" class="sex">
    		<label for="woman" class="sex_man">Женщина</label>
    		<br>


    		<button class="registration__button" type="submit" name="do_signup">Зарегистрироваться </button>
        </div>
     </form>


    	
  </fieldset>
  



  <!-- FOOTER -->
  	<footer class="footer">
  		<div class="footer__content">
  			<p class="footer__fraza">
				Какие же эти животные милые и приятные на вкус!!!
			</p>

			<?php
			require("../php/footer.php")
			?>