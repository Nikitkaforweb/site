<?php
  require "../db.php";

?>

<!DOCTYPE html>
<html>
<head>
	<title>Контакты</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/contacts.css">
	<link rel="stylesheet" type="text/css" href="../css/clogin.css">

	<!-- ШРИФТЫ -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ruslan+Display&amp;subset=cyrillic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Underdog&amp;subset=cyrillic" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
	<!-- СКРИПТЫ -->
	<script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script> 
	<script type="text/javascript" src="../js/popap.js"></script>

</head>
<body>
<!-- popap -->

<!-- <?php
	// require("../php/popap.php")
?> -->
	
<!-- end popap -->


  <!--HEADER-->
<?php
	require("../php/header.php")
?>

  <!--END OF HEADER-->

  <!--MENU-->

<?php
	require("../php/menu.php");
?>

  <!-- END OF MENU -->


<div class="body__for__maps">
	<div class="left__body__maps">
		<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d164153.5223038963!2d36.14574038135852!3d49.99450702373315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a09f63ab0f8b%3A0x2d4c18681aa4be0a!2sCharkiv%2C+Oblast&#39;+di+Charkiv%2C+Ucraina!5e0!3m2!1sit!2sit!4v1481713137170" width="350" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>

	<div class="right__body__maps">
		<div class="fio">
			<input type="text" name="fio" id="fio__posit">
			<label for="fio" id="label"><p>Введите ФИО:</p></label>
		</div>

		<div class="mobile">
			<input type="text" name="mobile__number" id="mobile__posit">
			<label for="mobile__number" id="label"><p>Введите ваш номер телефона: </p></label>
		</div>

		<div class="mail">
			<input type="mail" name="mail" id="mail__posit">
			<label for="mail" id="label"><p>Введите мэил:</p></label>
		</div>

		<div class="button__confirm">
			<button class="button__confirm__posit">Подтвердить</button>
		</div>
		
	</div>
</div>

    <!-- FOOTER -->
  	<footer class="footer">
  		<div class="footer__content">
  			<p class="footer__fraza">
				Одного любовь к животным делает человеком, другого — превращает в животное. 
			</p>

			<?php
			require("../php/footer.php")
			?>