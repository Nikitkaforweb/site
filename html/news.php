<?php
  require "../db.php";

?>

<!DOCTYPE html>
<html>
<head>
	<title>Новости</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/news.css">
	<link rel="stylesheet" type="text/css" href="../css/clogin.css">

	<!-- ШРИФТЫ -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ruslan+Display&amp;subset=cyrillic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Underdog&amp;subset=cyrillic" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
	<!-- СКРИПТЫ -->
	<script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script> 
	<script type="text/javascript" src="../js/popap.js"></script>

</head>
<body>



<!-- popap -->

<!-- <?php
	// require("../php/popap.php")
?> -->
	
<!-- end popap -->


  <!--HEADER-->
<?php
	require("../php/header.php")
?>

  <!--END OF HEADER-->

  <!--MENU-->

<?php
	require("../php/menu.php");
?>

  <!-- END OF MENU -->

		<div class="news">
			<div class="news__new">
				<p class="razdel">Новые</p>
				<fieldset class="news__font">
					<legend align="center"> Скидки </legend>
					<a href="../html/goods.php #griz"><img src="../img/akzia.jpg" height="100px" width="200px" class="news__img"></a>
					<a href="../html/goods.php #griz" class="news__text">При покупке корма для грызунов торговой марки Versele-Laga скидка -20%!</a>
				</fieldset>
			</div>

			<div class="news__older">
				<p class="razdel">Старее</p>
					<fieldset class="news__font">
						<legend align="center"> Акция </legend>
						<a href="../html/goods.php #cat"><img src="../img/sale.jpg" height="100px" width="200px" class="news__img"></a>
						<a href="../html/goods.php #cat" class="news__text">Выгодное предложение! Покупай 4шт. "ShinyCatFilet" от торговой марки Grim Cat по цене 3-х шт.!</a>
					</fieldset>
			</div>

			<div class="news__older__older">
			 	<p class="razdel">Старые</p>
				<fieldset class="news__font">
					<legend align="center"> Новинки </legend>
					<a href="../html/goods.php #dog"><img src="../img/new.jpg" height="100px" width="200px" class="news__img"></a>
					<a href="../html/goods.php #dog" class="news__text">Новинка! Корм для собак разной возрастной категории! GRANDORF! На 80% из натурального мяса!</a>
				</fieldset>
			</div>
		</div>

		 <!-- FOOTER -->
		  	<footer class="footer">
		  		<div class="footer__content">
		  			<p class="footer__fraza">
						Чтобы понять, есть ли у животных душа, надо самому иметь душу.
					</p>

					<?php
					require("../php/footer.php")
					?>