<style>
#slider-wrap{ 
	margin-top: 25px; 
	width:660px;
	height: 400px;
	margin-left:25% ;  
	}
#slider {
    overflow: hidden;
    width: 100%;
    height: 350px;
    position: relative;
    margin: 0 auto;
    width:660px;
	height: 400px;
}

#slider ul {
  position: relative;
  margin: 0;
  padding: 0;
  height: 200px;
  list-style: none;
}

#slider ul li {
  position: relative;
  display: inline;
  float: left;
  margin: 0;
  padding: 0;
  width: 960px;
  height: 300px;
}

a.btn_prev, a.btn_next {
  position: absolute;
  top: 0;
  z-index: 2;
  display: block;
  padding: 29% 3%;
  background: none; 
  color: orange;
  background: black;
  text-decoration: none;
  font-weight: 600;
  font-size: 1.3em;
  cursor: pointer;
}

a.btn_next { right: 0; }
a.btn_prev:focused, a.btn_next:focused { outline: none; }
</style>

<script>
jQuery(document).ready(function ($) {
    setInterval(function () {
        switchNext();
    }, 5000);
 
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
             $('#slider ul li:last-child').prependTo('#slider ul');

    function switchPrev() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 700, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function switchNext() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 700, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.btn_prev').click(function () {
        switchPrev();
    });

    $('a.btn_next').click(function () {
        switchNext();
    });
});

</script>


<div id="slider-wrap">
	<div id="slider">
          
          <ul>
            <li> <img src="img/001.jpg" height=" 400px" width="660px"> </li>
            <li> <img src="img/002.jpg" height="400px" width="660px"> </li>
            <li> <img src="img/003.jpg" height="400px" width="660px"> </li>
          </ul>
          <a href="#" class="btn_next"> >> </a>
          <a href="#" class="btn_prev"> << </a>
    </div> 
</div>
